/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Connector.Connectors {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
    import LiveContentArgumentType = Com.Wui.Framework.Services.Enums.LiveContentArgumentType;
    import FFIArgument = Com.Wui.Framework.Services.Connectors.FFIArgument;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import FFIArgumentCallback = Com.Wui.Framework.Services.Connectors.FFIArgumentCallback;

    export class FFIProxyTest extends UnitTestRunner {
        private ffiProxy : FFIProxy;
        private fileSystem : FileSystemHandler;
        private terminal : Terminal;
        private testResourceRoot : string;
        private libraryPath : string;
        private generator : string;

        constructor() {
            super();

            // this.setMethodFilter("testInputArgs");
            this.ffiProxy = new FFIProxy();
        }

        public testReturn() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const invokes : any[] = [];
                let index : number = 0;
                const getNextInvoke : any = () : void => {
                    if (index < invokes.length) {
                        LogIt.Debug("invoke " + (index + 1) + "/" + invokes.length);
                        invokes[index]();
                        index++;
                    } else {
                        $done();
                    }

                };

                invokes.push(() : void => {
                    // #1
                    this.ffiProxy
                        .InvokeMethod("ReturnInt", LiveContentArgumentType.RETURN_INT, ($returnValue : number) : void => {
                            assert.equal($returnValue, 23);
                            getNextInvoke();
                        });
                });
                invokes.push(() : void => {
                    // #2
                    this.ffiProxy
                        .InvokeMethod("ReturnBool", LiveContentArgumentType.RETURN_BOOLEAN, ($returnValue : boolean) : void => {
                            assert.equal($returnValue, true);
                            getNextInvoke();
                        });
                });
                invokes.push(() : void => {
                    // #3
                    this.ffiProxy
                        .InvokeMethod("ReturnFloat", LiveContentArgumentType.RETURN_FLOAT, ($returnValue : number) : void => {
                            assert.equal(Convert.ToFixed($returnValue, 12), 16.299999237061);
                            getNextInvoke();
                        });
                });
                invokes.push(() : void => {
                    // #4
                    this.ffiProxy
                        .InvokeMethod("ReturnDouble", LiveContentArgumentType.RETURN_DOUBLE, ($returnValue : number) : void => {
                            assert.equal(Convert.ToFixed($returnValue, 12), 33.44);
                            getNextInvoke();
                        });
                });
                invokes.push(() : void => {
                    // #5
                    this.ffiProxy
                        .InvokeMethod("ReturnString", LiveContentArgumentType.RETURN_STRING, ($returnValue : string) : void => {
                            assert.equal($returnValue, "returned string");
                            getNextInvoke();
                        });
                });

                getNextInvoke();
            };
        }

        public testInputArgs() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const invokes : any[] = [];
                let index : number = 0;
                const getNextInvoke : any = () : void => {
                    if (index < invokes.length) {
                        LogIt.Debug("invoke " + (index + 1) + "/" + invokes.length);
                        invokes[index]();
                        index++;
                    } else {
                        $done();
                    }

                };

                invokes.push(() : void => {
                    // #1
                    this.ffiProxy.InvokeMethod("CallWithEmpty", LiveContentArgumentType.RETURN_BOOLEAN,
                        ($returnValue : boolean) : void => {
                            assert.equal($returnValue, true);
                            getNextInvoke();
                        });
                });
                invokes.push(() : void => {
                    // #2
                    this.ffiProxy
                        .InvokeMethod("CallWithInt", LiveContentArgumentType.RETURN_BOOLEAN, 123,
                            ($returnValue : boolean) : void => {
                                assert.equal($returnValue, true);
                                getNextInvoke();
                            });
                });
                invokes.push(() : void => {
                    // #3
                    this.ffiProxy
                        .InvokeMethod("CallWithBool", LiveContentArgumentType.RETURN_BOOLEAN, true,
                            ($returnValue : boolean) : void => {
                                assert.equal($returnValue, true);
                                getNextInvoke();
                            });
                });
                invokes.push(() : void => {
                    // #4
                    this.ffiProxy
                        .InvokeMethod("CallWithFloat", LiveContentArgumentType.RETURN_BOOLEAN, <FFIArgument>{
                            type : LiveContentArgumentType.IN_FLOAT,
                            value: 3.14
                        }, ($returnValue : boolean) : void => {
                            assert.equal($returnValue, true);
                            getNextInvoke();
                        });
                });
                invokes.push(() : void => {
                    // #5
                    this.ffiProxy
                        .InvokeMethod("CallWithDouble", LiveContentArgumentType.RETURN_BOOLEAN, 6.28,
                            ($returnValue : boolean) : void => {
                                assert.equal($returnValue, true);
                                getNextInvoke();
                            });
                });
                invokes.push(() : void => {
                    // #6
                    this.ffiProxy
                        .InvokeMethod("CallWithString", LiveContentArgumentType.RETURN_BOOLEAN, <FFIArgument>{
                            type : LiveContentArgumentType.IN_STRING,
                            value: "hello world"
                        }, ($returnValue : boolean) : void => {
                            assert.equal($returnValue, true);
                            getNextInvoke();
                        });
                });
                invokes.push(() : void => {
                    // #7
                    this.ffiProxy
                        .InvokeMethod("CallWithString", LiveContentArgumentType.RETURN_BOOLEAN, "hello world",
                            ($returnValue : boolean) : void => {
                                assert.equal($returnValue, true);
                                getNextInvoke();
                            });
                });
                invokes.push(() : void => {
                    // #8
                    this.ffiProxy
                        .InvokeMethod("CallWithStruct", LiveContentArgumentType.RETURN_BOOLEAN, <FFIArgument>{
                                type : LiveContentArgumentType.IN_STRUCT,
                                value: {
                                    fields  : {
                                        a: 234,
                                        b: true,
                                        c: "struct string"
                                    },
                                    typeName: "TestStruct"
                                }
                            },
                            ($returnValue : boolean) : void => {
                                assert.equal($returnValue, true);
                                this.ffiProxy
                                    .InvokeMethod("CallWithStruct", LiveContentArgumentType.RETURN_BOOLEAN, <FFIArgument>{
                                            type : LiveContentArgumentType.IN_STRUCT,
                                            value: {
                                                fields  : {
                                                    a: 234,
                                                    b: true,
                                                    c: "struct string"
                                                },
                                                typeName: "TestStruct"
                                            }
                                        },
                                        ($returnValue : boolean) : void => {
                                            assert.equal($returnValue, true);
                                            getNextInvoke();
                                        });
                            });
                });
                invokes.push(() : void => {
                    // #9
                    this.ffiProxy
                        .InvokeMethod("CallWithIntArray", LiveContentArgumentType.RETURN_BOOLEAN, <FFIArgument>{
                                type : LiveContentArgumentType.IN_ARRAY,
                                value: {
                                    items   : [301, 303, 305, 407, 409, 502, 504, 806, 708, 900],
                                    typeName: LiveContentArgumentType.IN_INT
                                }
                            },
                            10,
                            ($returnValue : boolean) : void => {
                                assert.equal($returnValue, true);
                                this.ffiProxy
                                    .InvokeMethod("CallWithIntArray", LiveContentArgumentType.RETURN_BOOLEAN, <FFIArgument>{
                                            type : LiveContentArgumentType.IN_ARRAY,
                                            value: {
                                                items   : [301, 303, 305, 407, 409, 502, 504, 806, 708, 900],
                                                typeName: LiveContentArgumentType.IN_INT
                                            }
                                        },
                                        10,
                                        ($returnValue : boolean) : void => {
                                            assert.equal($returnValue, true);
                                            getNextInvoke();
                                        });
                            });
                });
                invokes.push(() : void => {
                    // #10
                    let callbackCalled = false;
                    this.ffiProxy.InvokeMethod("CallWithEmptyCallback", LiveContentArgumentType.RETURN_BOOLEAN, <FFIArgument>{
                            value: <FFIArgumentCallback>{
                                callback: () : void => {
                                    LogIt.Debug("native callback called");
                                    callbackCalled = true;
                                },
                                typeName: "EmptyCallback"
                            }
                        },
                        ($returnValue : boolean) : void => {
                            assert.ok($returnValue);
                            assert.ok(callbackCalled);
                            callbackCalled = false;
                            this.ffiProxy.InvokeMethod("CallWithEmptyCallback", LiveContentArgumentType.RETURN_BOOLEAN, <FFIArgument>{
                                    value: <FFIArgumentCallback>{
                                        callback: () : void => {
                                            LogIt.Debug("native callback called");
                                            callbackCalled = true;
                                        },
                                        typeName: "EmptyCallback"
                                    }
                                },
                                ($returnValue : boolean) : void => {
                                    assert.ok($returnValue);
                                    assert.ok(callbackCalled);
                                    getNextInvoke();
                                });
                        });
                });
                invokes.push(() : void => {
                    // #11
                    let callbackCalled : boolean = false;
                    this.ffiProxy.InvokeMethod("CallWithIntBoolCallback", LiveContentArgumentType.RETURN_INT, <FFIArgument>{
                            value: <FFIArgumentCallback>{
                                args      : [LiveContentArgumentType.IN_BOOLEAN],
                                callback  : ($status : boolean) : number => {
                                    callbackCalled = true;
                                    return 654;
                                },
                                returnType: LiveContentArgumentType.RETURN_INT,
                                typeName  : "IntBoolCallback"
                            }
                        },
                        ($returnValue : number) : void => {
                            assert.equal($returnValue, 654);
                            assert.ok(callbackCalled);
                            getNextInvoke();
                        });
                });
                invokes.push(() : void => {
                    // #12
                    let callbackCalled : boolean = false;
                    this.ffiProxy.InvokeMethod("CallWithIntBoolIntStringCallback", LiveContentArgumentType.RETURN_INT, <FFIArgument>{
                            value: <FFIArgumentCallback>{
                                args      : [
                                    LiveContentArgumentType.IN_BOOLEAN,
                                    LiveContentArgumentType.IN_INT,
                                    LiveContentArgumentType.IN_STRING
                                ],
                                callback  : ($status : boolean, $value : number, $data : string) : number => {
                                    callbackCalled = true;
                                    assert.equal($data, "callback string");
                                    return 666;
                                },
                                returnType: LiveContentArgumentType.RETURN_INT,
                                typeName  : "IntBoolIntString"
                            }
                        },
                        ($returnValue : number) : void => {
                            assert.equal($returnValue, 666);
                            assert.ok(callbackCalled);
                            getNextInvoke();
                        });
                });
                invokes.push(() : void => {
                    // #13
                    this.ffiProxy
                        .InvokeMethod("CallWithBuffer", LiveContentArgumentType.RETURN_BOOLEAN,
                            <FFIArgument>{
                                type : LiveContentArgumentType.IN_BUFFER,
                                value: [0, 1, 2, 3, 4, 5, 6, 7]
                            },
                            8,
                            ($returnValue : boolean) : void => {
                                assert.equal($returnValue, true);
                                getNextInvoke();
                            });
                });

                getNextInvoke();
            };
        }

        public testOutputArgs() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const invokes : any[] = [];
                let index : number = 0;
                const getNextInvoke : any = () : void => {
                    if (index < invokes.length) {
                        LogIt.Debug("invoke " + (index + 1) + "/" + invokes.length);
                        invokes[index]();
                        index++;
                    } else {
                        $done();
                    }

                };

                invokes.push(() : void => {
                    // #1
                    this.ffiProxy
                        .InvokeMethod("output_args_int", LiveContentArgumentType.RETURN_VOID, <FFIArgument>{
                            type : LiveContentArgumentType.OUT_INT,
                            value: 111
                        }, ($returnValue : any, $output : number) : void => {
                            assert.equal($returnValue, LiveContentArgumentType.RETURN_VOID);
                            assert.equal($output, 15);
                            getNextInvoke();
                        });
                });
                invokes.push(() : void => {
                    // #2
                    this.ffiProxy
                        .InvokeMethod("output_args_char", LiveContentArgumentType.RETURN_VOID, <FFIArgument>{
                            type : LiveContentArgumentType.OUT_STRING,
                            value: "hello world"
                        }, ($returnValue : any, $output : string) : void => {
                            assert.equal($returnValue, LiveContentArgumentType.RETURN_VOID);
                            assert.equal($output, "some text");
                            getNextInvoke();
                        });
                });
                invokes.push(() : void => {
                    // #3
                    this.ffiProxy
                        .InvokeMethod("output_args_char", LiveContentArgumentType.RETURN_VOID, <FFIArgument>{
                            type : LiveContentArgumentType.OUT_STRING,
                            value: 10
                        }, ($returnValue : any, $output : string) : void => {
                            assert.equal($returnValue, LiveContentArgumentType.RETURN_VOID);
                            assert.equal($output, "some text");
                            getNextInvoke();
                        });
                });
                invokes.push(() : void => {
                    // #4
                    this.ffiProxy
                        .InvokeMethod("output_args_float", LiveContentArgumentType.RETURN_VOID, <FFIArgument>{
                            type : LiveContentArgumentType.OUT_FLOAT,
                            value: 1.1
                        }, ($returnValue : number, $data : number) : void => {
                            assert.equal($returnValue, LiveContentArgumentType.RETURN_VOID);
                            assert.equal(Convert.ToFixed($data, 12), 12.119999885559);
                            getNextInvoke();
                        });
                });
                invokes.push(() : void => {
                    // #5
                    this.ffiProxy
                        .InvokeMethod("output_args_double", LiveContentArgumentType.RETURN_VOID, <FFIArgument>{
                            type : LiveContentArgumentType.OUT_DOUBLE,
                            value: 6.66
                        }, ($returnValue : number, $data : number) : void => {
                            assert.equal($returnValue, LiveContentArgumentType.RETURN_VOID);
                            assert.equal(Convert.ToFixed($data, 12), 56.119998931885);
                            getNextInvoke();
                        });
                });
                invokes.push(() : void => {
                    // #6
                    this.ffiProxy
                        .InvokeMethod("output_args_bool", LiveContentArgumentType.RETURN_VOID, <FFIArgument>{
                            type : LiveContentArgumentType.OUT_BOOLEAN,
                            value: true
                        }, ($returnValue : any, $data : boolean) : void => {
                            assert.equal($returnValue, LiveContentArgumentType.RETURN_VOID);
                            assert.equal($data, false);
                            getNextInvoke();
                        });
                });
                invokes.push(() : void => {
                    // #7
                    this.ffiProxy
                        .InvokeMethod("output_args_buffer", LiveContentArgumentType.RETURN_VOID, <FFIArgument>{
                            type : LiveContentArgumentType.OUT_BUFFER,
                            value: 20
                        }, ($returnValue : any, $data : any) : void => {
                            assert.equal($returnValue, LiveContentArgumentType.RETURN_VOID);
                            assert.deepEqual($data, Buffer.from([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]));
                            getNextInvoke();
                        });
                });

                getNextInvoke();
            };
        }

        public testVarious() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const invokes : any[] = [];
                let index : number = 0;
                const getNextInvoke : any = () : void => {
                    if (index < invokes.length) {
                        LogIt.Debug("invoke " + (index + 1) + "/" + invokes.length);
                        invokes[index]();
                        index++;
                    } else {
                        $done();
                    }

                };

                invokes.push(() : void => {
                    // #1
                    this.ffiProxy
                        .InvokeMethod("int_func_int_bool", LiveContentArgumentType.RETURN_INT, 43, true,
                            ($returnValue : number) : void => {
                                assert.equal($returnValue, 29);
                                getNextInvoke();
                            });
                });
                invokes.push(() : void => {
                    // #2
                    this.ffiProxy
                        .InvokeMethod("int_func_bool_int", LiveContentArgumentType.RETURN_INT, false, 44,
                            ($returnValue : number) : void => {
                                assert.equal($returnValue, 23);
                                getNextInvoke();
                            });
                });
                invokes.push(() : void => {
                    // #3
                    this.ffiProxy
                        .InvokeMethod("add_input_ints", LiveContentArgumentType.RETURN_INT, 11, 21,
                            ($returnValue : number) : void => {
                                assert.equal($returnValue, 32);
                                getNextInvoke();
                            });
                });
                invokes.push(() : void => {
                    // #4
                    this.ffiProxy
                        .InvokeMethod("output_args_int_bool_float", LiveContentArgumentType.RETURN_INT,
                            <FFIArgument>{
                                type : LiveContentArgumentType.OUT_INT,
                                value: 11
                            },
                            <FFIArgument>{
                                type : LiveContentArgumentType.OUT_BOOLEAN,
                                value: true
                            },
                            <FFIArgument>{
                                type : LiveContentArgumentType.OUT_FLOAT,
                                value: 36.98
                            }, ($returnValue : number, $intOut : number, $boolOut : boolean, $floatOut : number) : void => {
                                assert.equal($returnValue, 3);
                                assert.equal($intOut, 12);
                                assert.equal($boolOut, false);
                                assert.equal(Convert.ToFixed($floatOut, 12), 3.140000104904);
                                getNextInvoke();
                            });
                });
                invokes.push(() : void => {
                    // #5
                    this.ffiProxy
                        .InvokeMethod("input_int_double_output_bool_int_input_string", LiveContentArgumentType.RETURN_INT,
                            78,
                            <FFIArgument>{
                                value: 79.5
                            },
                            <FFIArgument>{
                                type : LiveContentArgumentType.OUT_BOOLEAN,
                                value: true
                            },
                            <FFIArgument>{
                                type : LiveContentArgumentType.OUT_INT,
                                value: 17
                            },
                            <FFIArgument>{
                                type : LiveContentArgumentType.IN_STRING,
                                value: "last one"
                            }, ($returnValue : number, $boolOut : boolean, $intOut : number) : void => {
                                assert.equal($returnValue, 8);
                                assert.equal($boolOut, false);
                                assert.equal($intOut, 432);
                                getNextInvoke();
                            });
                });
                invokes.push(() : void => {
                    // #6
                    this.ffiProxy
                        .InvokeMethod("append_string", LiveContentArgumentType.RETURN_INT, "hello ", "world",
                            <FFIArgument>{type: LiveContentArgumentType.OUT_STRING, value: 20},
                            ($returnValue : number, $output : string) : void => {
                                assert.equal($returnValue, 11);
                                assert.equal($output, "hello world");
                                getNextInvoke();
                            });
                });

                getNextInvoke();
            };
        }

        protected before() : IUnitTestRunnerPromise {
            this.timeoutLimit(100000);
            return ($done : () => void) : void => {
                this.testResourceRoot = this.getAbsoluteRoot() + "/test/resource/data/Com/Wui/Framework/Connector/Connectors/FFIProxyLib";

                if (EnvironmentHelper.IsWindows()) {
                    // windows
                    this.generator = "MinGW Makefiles";
                    this.libraryPath = "libFFIProxyLib.dll";
                } else {
                    // linux
                    this.generator = "Unix Makefiles";
                    this.libraryPath = "libFFIProxyLib.so";
                }
                this.libraryPath = this.testResourceRoot + "/build/" + this.libraryPath;

                this.fileSystem = Loader.getInstance().getFileSystemHandler();
                this.terminal = Loader.getInstance().getTerminal();

                const loadLib : any = () : void => {
                    this.ffiProxy.Load(this.libraryPath, ($status : boolean, $libPath : string) : void => {
                        if (!$status) {
                            LogIt.Error("Failed to load library: " + $libPath);
                        }
                        $done();
                    });
                };

                if (this.fileSystem.Exists(this.libraryPath)) {
                    LogIt.Debug("Library already exists, build skipped.");
                    loadLib();
                } else {
                    if (this.fileSystem.CreateDirectory(this.testResourceRoot + "/build_cache")) {
                        this.terminal.Spawn(
                            "cmake",
                            ["-G", "\"" + this.generator + "\"", ".."],
                            this.testResourceRoot + "/build_cache",
                            ($exitCode : number, $std : string[]) : void => {
                                if ($exitCode !== 0) {
                                    LogIt.Error("Build failed:\n" + $std[0] + $std[1]);
                                } else {
                                    this.terminal.Spawn(
                                        "cmake",
                                        ["--build", "."],
                                        this.testResourceRoot + "/build_cache",
                                        ($exitCode : number, $std : string[]) : void => {
                                            if ($exitCode === 0) {
                                                loadLib();
                                            } else {
                                                LogIt.Error("Can not build library:\n" + $std[0] + $std[1]);
                                            }
                                        }
                                    );
                                }
                            });
                    } else {
                        LogIt.Error("Failed to prepare build cache for test resource.");
                    }
                }
            };
        }
    }
}
