/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Connector.Connectors {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class NetworkTest extends UnitTestRunner {

        public testIsProxyRequired() : IUnitTestRunnerPromise {
            const network : Network = new Network();
            return ($done : () => void) : void => {
                network.IsProxyRequired(($status : boolean) : void => {
                    assert.equal($status, false);
                    $done();
                });
            };
        }
    }
}
