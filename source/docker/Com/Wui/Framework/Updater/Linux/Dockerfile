 #/* ********************************************************************************************************* *
 # *
 # * Copyright (c) 2018-2019 NXP
 # *
 # * SPDX-License-Identifier: BSD-3-Clause
 # * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 # * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 # *
 # * ********************************************************************************************************* */

FROM wuiframework/wui-ubuntu-base:latest
MAINTAINER Michal Kelnar "michal.kelnar@nxp.com"

ARG WUI_HUB_HOSTNAME="hub.wuiframework.com"

ARG WUI_CONNECTOR_VERSION=""
ARG WUI_CONNECTOR_PATH="/var/wui/com-wui-framework-updater"

ENV WUI_CONNECTOR_PATH ${WUI_CONNECTOR_PATH}
ENV WUI_CONNECTOR_VERSION ${WUI_CONNECTOR_VERSION}

RUN mkdir -p ${WUI_CONNECTOR_PATH}

WORKDIR ${WUI_CONNECTOR_PATH}

RUN curl -fsSL --compressed "https://${WUI_HUB_HOSTNAME}/Update/com-wui-framework-connector/Linux/app-linux-nodejs/${WUI_CONNECTOR_VERSION}" > connector.tar.gz && \
    tar xf connector.tar.gz && \
    cd com-wui-framework-connector-* && \
    cp -r . ../ && \
    cd .. && \
    rm -rf com-wui-framework-connector-* && \
    rm connector.tar.gz

COPY WuiConnector.config.jsonp ${WUI_CONNECTOR_PATH}/WuiConnector.config.jsonp

CMD ["/sbin/my_init", "--", "./WuiConnector", "agent"]

