#!/bin/bash
# * ********************************************************************************************************* *
# *
# * Copyright (c) 2019 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

sourceFile="${BASH_SOURCE[0]}"
while [ -h "$sourceFile" ]; do
    sourceLink="$(readlink "$sourceFile")"
    if [[ $sourceLink == /* ]]; then
        sourceFile="$sourceLink"
    else
        sourceDir="$( dirname "$sourceFile" )"
        sourceFile="$sourceDir/$sourceLink"
    fi
done
binBase="$( cd -P "$( dirname "$sourceFile" )" && pwd )"

mkdir -p /var/wui/apps
mkdir -p /var/wui/com-wui-framework-updater
mkdir -p /var/lib/com-wui-framework-updater
mkdir -p /var/lib/com-wui-framework-connector

cd /var/wui/com-wui-framework-updater

curl -fsSL --compressed "https://hub.wuiframework.com/Update/com-wui-framework-connector/i.MX/app-imx-nodejs" > updater.tar.gz
tar xf updater.tar.gz
cd com-wui-framework-connector-*
cp -r . ../
cd ..
rm -rf com-wui-framework-connector-*
rm updater.tar.gz

cp $binBase/WuiConnector.config.jsonp /var/wui/com-wui-framework-updater/WuiConnector.config.jsonp
cp $binBase/WuiConnector.config.jsonp.template /var/lib/com-wui-framework-updater/WuiConnector.config.jsonp

cd /var/lib/com-wui-framework-updater
 
ownHash=$(cat /sys/class/net/eth0/address | md5sum | awk '{print $1}')
export ownHash=$ownHash
echo "#!/bin/bash
cat > tmp.sh << EOF
`cat WuiConnector.config.jsonp`
 " > tmp.sh;
chmod +x tmp.sh;
./tmp.sh > /dev/null 2>/dev/null
cat tmp.sh > WuiConnector.config.jsonp
rm tmp.sh

cp $binBase/wui_updater.service /etc/systemd/system/wui_updater.service
systemctl enable wui_updater.service
systemctl start wui_updater.service
