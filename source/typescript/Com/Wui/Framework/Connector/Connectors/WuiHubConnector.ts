/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Connector.Connectors {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import WebServiceClientType = Com.Wui.Framework.Services.Enums.WebServiceClientType;
    import WebServiceConfiguration = Com.Wui.Framework.Commons.WebServiceApi.WebServiceConfiguration;
    import ILiveContentProtocol = Com.Wui.Framework.Services.Interfaces.ILiveContentProtocol;
    import AgentsRegisterConnector = Com.Wui.Framework.Services.Connectors.AgentsRegisterConnector;
    import IAgentCapabilities = Com.Wui.Framework.Services.Connectors.IAgentCapabilities;
    import LiveContentWrapper = Com.Wui.Framework.Services.WebServiceApi.LiveContentWrapper;
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import IAcknowledgePromise = Com.Wui.Framework.Services.Connectors.IAcknowledgePromise;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;

    export class WuiHubConnector extends AgentsRegisterConnector {

        constructor($reconnect : boolean = true, $serverConfigurationSource? : string | WebServiceConfiguration) {
            super($reconnect, $serverConfigurationSource);
        }

        public RegisterAgent($capabilities : IAgentCapabilities) : IConnectorHubRegisterPromise {
            const callbacks : any = {
                onComplete($success : boolean) : any {
                    // declare default callback
                },
                onError: null,
                onMessage($data : ILiveContentProtocol, $taskId : string) : any {
                    // declare default callback
                }
            };
            LiveContentWrapper.InvokeMethod(this.getClient(), this.getServerNamespace(),
                "RegisterAgent", $capabilities)
                .OnError(($error : ErrorEventArgs, ...$args : any[]) : void => {
                    this.errorHandler(callbacks.onError, $error, $args);
                })
                .Then(($result : any) : void => {
                    if (ObjectValidator.IsSet($result.type)) {
                        if ($result.type === EventType.ON_CHANGE) {
                            const data : IWuiConnectorRequestProtocol = $result.data;
                            callbacks.onMessage(data.protocol, data.taskId);
                        }
                    } else if (ObjectValidator.IsBoolean($result)) {
                        callbacks.onComplete($result);
                    }
                });
            const promise : IConnectorHubRegisterPromise = {
                OnError  : ($callback : ($args : ErrorEventArgs) => void) : IAcknowledgePromise => {
                    callbacks.onError = $callback;
                    return promise;
                },
                OnMessage: ($callback : ($data : ILiveContentProtocol, $taskId : string) => void) : IAcknowledgePromise => {
                    callbacks.onMessage = $callback;
                    return promise;
                },
                Then     : ($callback : ($success : boolean) => void) : void => {
                    callbacks.onComplete = $callback;
                }
            };
            return promise;
        }

        protected getServerNamespaces() : any {
            const namespaces : any = {};
            namespaces[WebServiceClientType.WUI_HUB_CONNECTOR] = "Com.Wui.Framework.Hub.Utils.WuiConnectorHub";
            return namespaces;
        }

        protected init($serverConfigurationSource? : string | WebServiceConfiguration, $clientType? : WebServiceClientType) : void {
            if (ObjectValidator.IsEmptyOrNull($serverConfigurationSource)) {
                $serverConfigurationSource = Loader.getInstance().getAppConfiguration().agent.hub + "/connector.config.jsonp";
            }
            super.init($serverConfigurationSource, $clientType);
        }
    }

    export abstract class IWuiConnectorRequestProtocol {
        public protocol : ILiveContentProtocol;
        public taskId : string;
    }

    export interface IConnectorHubRegisterPromise extends Com.Wui.Framework.Services.Connectors.IAcknowledgePromise {
        OnMessage($callback : ($data : ILiveContentProtocol, $taskId : string) => void) : IAcknowledgePromise;
    }
}
