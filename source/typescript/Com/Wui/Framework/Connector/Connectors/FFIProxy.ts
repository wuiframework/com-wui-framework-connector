/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Connector.Connectors {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import LiveContentArgumentType = Com.Wui.Framework.Services.Enums.LiveContentArgumentType;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
    import BaseResponse = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.Handlers.BaseResponse;
    import BaseConnector = Com.Wui.Framework.Localhost.Primitives.BaseConnector;
    import FFIOptions = Com.Wui.Framework.Services.Connectors.FFIOptions;
    import FFIArgument = Com.Wui.Framework.Services.Connectors.FFIArgument;
    import FFIArgumentStruct = Com.Wui.Framework.Services.Connectors.FFIArgumentStruct;
    import Extern = Com.Wui.Framework.Localhost.Primitives.Extern;

    export class FFIProxy extends BaseConnector {
        private library : any;
        private cwdBackup : string;
        private envBackup : any;

        private static getTypeFromValue($value : any) : string {
            let retVal : string = "void";
            if (ObjectValidator.IsObject($value)) {
                if (!ObjectValidator.IsEmptyOrNull($value.fields)) {
                    retVal = "struct";
                }
                if (!ObjectValidator.IsEmptyOrNull($value.callback)) {
                    retVal = "callback";
                }
                if (!ObjectValidator.IsEmptyOrNull($value.items)) {
                    retVal = "array";
                }
            } else if (ObjectValidator.IsString($value)) {
                retVal = Com.Wui.Framework.Connector.Connectors.FFIProxy.translateType($value);
                if (retVal === "void") {
                    retVal = "char*";
                }
            } else if (ObjectValidator.IsInteger($value)) {
                retVal = "int";
            } else if (ObjectValidator.IsDouble($value)) {
                retVal = "double";
            } else if (ObjectValidator.IsBoolean($value)) {
                retVal = "bool";
            }
            return retVal;
        }

        private static translateType($type : LiveContentArgumentType) : string {
            let translated : string;

            switch ($type) {
            case LiveContentArgumentType.IN_STRING:
            case LiveContentArgumentType.OUT_STRING:
            case LiveContentArgumentType.RETURN_STRING:
                translated = "char*";
                break;
            case LiveContentArgumentType.IN_BUFFER:
            case LiveContentArgumentType.OUT_BUFFER:
                translated = "buffer";
                break;
            case LiveContentArgumentType.IN_BOOLEAN:
            case LiveContentArgumentType.OUT_BOOLEAN:
            case LiveContentArgumentType.RETURN_BOOLEAN:
                translated = "bool";
                break;
            case LiveContentArgumentType.IN_INT:
            case LiveContentArgumentType.OUT_INT:
            case LiveContentArgumentType.RETURN_INT:
                translated = "int";
                break;
            case LiveContentArgumentType.IN_DOUBLE:
            case LiveContentArgumentType.OUT_DOUBLE:
            case LiveContentArgumentType.RETURN_DOUBLE:
                translated = "double";
                break;
            case LiveContentArgumentType.IN_FLOAT:
            case LiveContentArgumentType.OUT_FLOAT:
            case LiveContentArgumentType.RETURN_FLOAT:
                translated = "float";
                break;
            case LiveContentArgumentType.IN_STRUCT:
                translated = "struct";
                break;
            case LiveContentArgumentType.IN_ARRAY:
            case LiveContentArgumentType.OUT_ARRAY:
                translated = "array";
                break;
            default:
                translated = "void";
                break;
            }
            return translated;
        }

        private static changeCwd($path : string) : void {
            try {
                LogIt.Info("Changing working directory to " + $path);
                process.chdir($path);
            } catch ($ex) {
                LogIt.Error($ex);
            }
        }

        @Extern()
        public Load($fileOrOptions : string | FFIOptions,
                    $callback? : (($status : boolean, $libraryPath : string) => void) | IResponse) : void {
            const response : IResponse = ResponseFactory.getResponse($callback);
            if (this.isSupported()) {
                const fileSystem : FileSystemHandler = Loader.getInstance().getFileSystemHandler();
                try {
                    const Library : any = require("fastcall").Library;
                    let options : FFIOptions = <FFIOptions>{};

                    this.Release();

                    if (ObjectValidator.IsString($fileOrOptions)) {
                        options.libraryPath = <string>$fileOrOptions;
                    } else {
                        options = <FFIOptions>$fileOrOptions;
                    }

                    if (!ObjectValidator.IsEmptyOrNull(options.env)) {
                        this.envBackup = process.env;
                        let key : string;
                        for (key in options.env) {
                            if (options.env.hasOwnProperty(key)) {
                                process.env[key] = fileSystem.NormalizePath(options.env[key].replace(
                                    EnvironmentHelper.IsWindows() ? /%([a-zA-Z0-9_]+)%/g : /\$([a-zA-Z0-9_]+)/g,
                                    ($match : any, $key : any) : string => {
                                        let retVal : string = process.env[$key];
                                        if (ObjectValidator.IsEmptyOrNull(retVal)) {
                                            retVal = $match;
                                        }
                                        return retVal;
                                    }));
                                LogIt.Debug("Env key {0} changed to {1}", key, process.env[key]);
                            }
                        }
                    }

                    if (!ObjectValidator.IsEmptyOrNull(options.cwd)) {
                        if (fileSystem.Exists(options.cwd)) {
                            this.cwdBackup = Loader.getInstance().getProgramArgs().TargetBase();
                            Com.Wui.Framework.Connector.Connectors.FFIProxy.changeCwd(options.cwd);
                        } else {
                            LogIt.Warning("Working directory change skipped because path \"" + options.cwd + "\" not exists.");
                        }
                    }

                    this.library = new Library(fileSystem.NormalizePath(options.libraryPath));

                    response.Send(true, options.libraryPath);
                } catch (ex) {
                    LogIt.Warning(ex.message);
                    response.Send(false);
                }
            } else {
                response.OnError("FFI proxy is not supported by current Connector instance.");
            }
        }

        @Extern(false)
        public InvokeMethod($name : string, $returnType : any, ...$args : any[]) : void {
            let response : IResponse;

            const returnArgs : any[] = [];
            let methodExists : boolean = false;
            if (!ObjectValidator.IsEmptyOrNull($args)) {
                const lastArg : any = $args[$args.length - 1];
                if (ObjectValidator.IsFunction(lastArg) ||
                    (ObjectValidator.IsObject(lastArg) && Reflection.getInstance().IsMemberOf(lastArg, BaseResponse))) {
                    response = ResponseFactory.getResponse(lastArg);
                    $args.splice(-1, 1);
                }
            }
            if (ObjectValidator.IsEmptyOrNull(response)) {
                response = ResponseFactory.getResponse(() : void => {
                    // default response
                });
            }
            if (!ObjectValidator.IsEmptyOrNull(this.library)) {
                const fastcall : any = require("fastcall");
                const ref : any = fastcall.ref;
                const functionArgs : any[] = [];
                const outArgs : any[] = [];
                let argsDeclaration : string = "";

                const constructChar : any = ($value : any, $isReadOnly? : boolean) : any => {
                    let retVal : any;
                    if (ObjectValidator.IsEmptyOrNull($isReadOnly) || !$isReadOnly || !ObjectValidator.IsString($value)) {
                        retVal = ref.allocCString(ObjectValidator.IsString($value) ?
                            $value : Buffer.alloc(<number>$value).toString());
                    } else {
                        retVal = fastcall.makeStringBuffer($value);
                    }

                    return retVal;
                };

                const constructStructDefinition : any = ($struct : FFIArgumentStruct) : string => {
                    let decl : string = "struct " + $struct.typeName + " { <?>; }";
                    const def : any = {};
                    let field : any = {};
                    for (field in $struct.fields) {
                        if ($struct.fields.hasOwnProperty(field)) {
                            decl = decl.replace(/<\?>;/g, ($match : string) : string => {
                                let retVal : string;
                                if (ObjectValidator.IsObject($struct.fields[field])) {
                                    if (ObjectValidator.IsEmptyOrNull($struct.fields[field].type) ||
                                        ObjectValidator.IsEmptyOrNull($struct.fields[field].value)) {
                                        return $match;
                                    }
                                    def[field] = $struct.fields[field].value;
                                    retVal = Com.Wui.Framework.Connector.Connectors.FFIProxy.translateType($struct.fields[field].type);
                                } else {
                                    let value : any = $struct.fields[field];
                                    retVal = Com.Wui.Framework.Connector.Connectors.FFIProxy.getTypeFromValue(value);
                                    if (retVal === "void" || retVal === "char*") {
                                        retVal = "char*";
                                        value = constructChar(value);
                                    }
                                    def[field] = value;
                                }
                                return retVal + " " + field + "; " + $match;
                            });
                        }
                    }
                    if (!this.library.structs.hasOwnProperty($struct.typeName)) {
                        this.library.struct(decl.replace(/\s<\?>;/g, ""));
                    }
                    return def;
                };

                if (!ObjectValidator.IsEmptyOrNull($args)) {
                    let index : number;
                    for (index = 0; index < $args.length; index++) {
                        let arg : any = $args[index];
                        let outValue : any;
                        let isOutput : boolean = false;
                        let type : string = "";

                        if (ObjectValidator.IsObject(arg)) {
                            const ffiArg : FFIArgument = <FFIArgument>arg;
                            if (!ObjectValidator.IsEmptyOrNull(ffiArg.type)) {
                                if (StringUtils.StartsWith(<string>ffiArg.type, "__[OUT:")) {
                                    isOutput = true;
                                }
                                type = Com.Wui.Framework.Connector.Connectors.FFIProxy.translateType(ffiArg.type);

                                if (ObjectValidator.IsSet(ffiArg.value)) {
                                    outValue = ffiArg.value;
                                    arg = outValue;
                                }
                            } else if (ObjectValidator.IsSet(ffiArg.value)) {
                                outValue = ffiArg.value;
                                arg = outValue;
                            }
                        } else if (ObjectValidator.IsFunction(arg)) {
                            LogIt.Error("Function argument is not supported. To use callback please define in " +
                                "FFIArgumentCallback structure.");
                        }
                        if (StringUtils.IsEmpty(type)) {
                            type = Com.Wui.Framework.Connector.Connectors.FFIProxy.getTypeFromValue(arg);
                        }

                        if (isOutput) {
                            let outArg : any;
                            if (ObjectValidator.IsSet(outValue)) {
                                if (type === "char*") {
                                    outArg = constructChar(outValue, true);
                                } else if (type === "buffer") {
                                    outArg = Buffer.alloc(<number>outValue);
                                    outValue = "buffer";
                                    type = "char*";
                                } else {
                                    outArg = ref.alloc(type, outValue);
                                    type += "*";
                                }
                            } else {
                                outValue = null;
                                outArg = ref.refType(type);
                            }
                            outArgs.push({
                                input  : outValue,
                                pointer: outArg,
                                type
                            });
                            functionArgs.push(outArg);
                        } else if (type === "char*") {
                            functionArgs.push(constructChar(arg));
                        } else if (type === "struct") {
                            if (ObjectValidator.IsObject(outValue)) {
                                const structDecl : FFIArgumentStruct = <FFIArgumentStruct>outValue;
                                type = structDecl.typeName + "*";
                                functionArgs.push(constructStructDefinition(structDecl));
                            } else {
                                LogIt.Error("Argument type struct requires full declaration of demand type.");
                            }
                        } else if (type === "callback") {
                            const retType : string = Com.Wui.Framework.Connector.Connectors.FFIProxy.translateType(arg.returnType);
                            const args : string[] = [];
                            if (!ObjectValidator.IsEmptyOrNull(arg.args)) {
                                arg.args.forEach(($item : any) : void => {
                                    args.push(Com.Wui.Framework.Connector.Connectors.FFIProxy.translateType($item));
                                });
                            }
                            const decl : string = retType + " (*" + arg.typeName + ")(" + args.join(", ") + ")";

                            if (!this.library.callbacks.hasOwnProperty(arg.typeName)) {
                                this.library.callback(decl);
                            }

                            const tCallback : any = (...$args : any[]) : any => {
                                const args : any[] = [];
                                for (let i = 0; i < $args.length; i++) {
                                    if ($args.hasOwnProperty(i)) {
                                        if (Com.Wui.Framework.Connector.Connectors.FFIProxy.translateType(arg.args[i]) === "char*") {
                                            args.push($args[i].readCString());
                                        } else {
                                            args.push($args[i]);
                                        }
                                    }
                                }
                                return arg.callback.apply(this, args);
                            };

                            functionArgs.push(tCallback);
                            type = arg.typeName;
                        } else if (type === "array") {
                            const key : string = "type" + StringUtils.getSha1($name + JSON.stringify(arg));
                            const decl : string = Com.Wui.Framework.Connector.Connectors.FFIProxy.translateType(arg.typeName) + "[] " + key;
                            if (!this.library.arrays.hasOwnProperty(key)) {
                                this.library.array(decl);
                            }
                            functionArgs.push(ObjectValidator.IsArray(arg.items) ? arg.items : Array(arg.items));
                            type = key;
                        } else if (type === "buffer") {
                            type = "char*";
                            functionArgs.push(ObjectValidator.IsArray(arg) ? Buffer.from(arg) : Buffer.alloc(<number>arg));
                        } else {
                            functionArgs.push(arg);
                        }
                        argsDeclaration += type;
                        if (index + 1 < $args.length) {
                            argsDeclaration += ",";
                        }
                    }
                }
                if (!this.library.interface.hasOwnProperty($name)) {
                    const declaration : string = FFIProxy.translateType($returnType) + " " + $name + "(" + argsDeclaration + ")";
                    LogIt.Debug("> declare interface: {0}", declaration);
                    try {
                        this.library.function(declaration);
                        methodExists = true;
                    } catch (ex) {
                        LogIt.Warning(ex.message);
                        response.OnError("Required interface \"" + declaration + "\" was not found.");
                    }
                } else {
                    methodExists = true;
                }
                if (methodExists) {
                    LogIt.Debug("Function args length: {0}", functionArgs.length);
                    this.library.interface[$name].async.apply(this.library.interface, functionArgs)
                        .then(($returnValue : any) : void => {
                            outArgs.forEach(($outArg : any) : void => {
                                if ($outArg.input === "buffer") {
                                    returnArgs.push($outArg.pointer);
                                } else if ($outArg.type === "char*") {
                                    returnArgs.push(ref.readCString($outArg.pointer));
                                } else {
                                    returnArgs.push($outArg.pointer.deref());
                                }
                            });

                            if ($returnType === LiveContentArgumentType.RETURN_VOID && ObjectValidator.IsEmptyOrNull($returnValue)) {
                                $returnValue = LiveContentArgumentType.RETURN_VOID;
                            } else if ($returnType === LiveContentArgumentType.RETURN_STRING) {
                                $returnValue = ref.readCString($returnValue);
                            }
                            response.Send.apply(response, [$returnValue].concat(returnArgs));
                        });
                } else {
                    response.Send.apply(response, [LiveContentArgumentType.RETURN_VOID]);
                }
            } else {
                response.OnError("Can't invoke method, because instance of required library has not been found.");
            }
        }

        @Extern()
        public Release() : void {
            if (!ObjectValidator.IsEmptyOrNull(this.library)) {
                this.library.release();
                this.library = null;
            }

            if (!ObjectValidator.IsEmptyOrNull(this.envBackup)) {
                process.env = this.envBackup;
                this.envBackup = null;
            }

            if (!ObjectValidator.IsEmptyOrNull(this.cwdBackup)) {
                Com.Wui.Framework.Connector.Connectors.FFIProxy.changeCwd(this.cwdBackup);
                this.cwdBackup = "";
            }
        }

        private isSupported() : boolean {
            let status : boolean = false;
            try {
                require("fastcall");
                status = true;
            } catch (e) {
                // fastcall is missing
                LogIt.Debug("Fastcall load failed: {0}", e.stack);
            }
            this.isSupported = () : boolean => {
                return status;
            };
            return status;
        }
    }
}
