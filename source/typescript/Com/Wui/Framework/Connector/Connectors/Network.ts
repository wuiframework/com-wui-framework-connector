/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Connector.Connectors {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import BaseConnector = Com.Wui.Framework.Localhost.Primitives.BaseConnector;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Extern = Com.Wui.Framework.Localhost.Primitives.Extern;
    import child_process = Types.NodeJS.Modules.child_process;

    export class Network extends BaseConnector {

        @Extern()
        public getFreePort($callback : (($value : number) => void) | IResponse) : void {
            const getPort : any = require("get-port");
            getPort().then(($port) : void => {
                ResponseFactory.getResponse($callback).Send($port);
            });
        }

        public IsPortFree($value : number, $callback : (($status : boolean) => void) | IResponse) : void {
            const isPortFree : any = require("is-port-free");
            isPortFree($value)
                .then(() : void => {
                    ResponseFactory.getResponse($callback).Send(true);
                })
                .catch(() : void => {
                    ResponseFactory.getResponse($callback).Send(false);
                });
        }

        public IsProxyRequired($callback : (($status : boolean) => void) | IResponse) : void {
            const response : IResponse = ResponseFactory.getResponse($callback);
            Loader.getInstance().getHttpManager().IsOnline(($status : boolean) : void => {
                if ($status) {
                    Loader.getInstance().getFileSystemHandler().Download({
                        streamOutput: true,
                        url         : "https://hub.wuiframework.com/ping.txt"
                    }, ($headers : string, $body : string) : void => {
                        if (StringUtils.IsEmpty($body)) {
                            response.Send(false);
                        } else {
                            let httpProxy : string = Loader.getInstance().getAppConfiguration().httpProxy;
                            if (ObjectValidator.IsEmptyOrNull(httpProxy)) {
                                response.Send(true);
                            } else {
                                if (!StringUtils.StartsWith(httpProxy, "http://")) {
                                    httpProxy = "http://" + httpProxy;
                                }
                                if (!StringUtils.EndsWith(httpProxy, "/")) {
                                    httpProxy += "/";
                                }
                                const retVal : child_process.SpawnSyncReturns<Buffer> = require("child_process").spawnSync(
                                    "ping " + require("url").parse(httpProxy).hostname + " -n 1", {
                                        shell      : true,
                                        windowsHide: false
                                    });
                                response.Send(retVal.status !== 0);
                            }
                        }
                    });
                } else {
                    response.Send(false);
                }
            });
        }
    }
}
