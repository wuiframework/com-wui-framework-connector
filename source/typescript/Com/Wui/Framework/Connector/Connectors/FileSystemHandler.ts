/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Connector.Connectors {
    "use strict";
    import ResponseFactory = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.ResponseFactory;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import IFileSystemItemProtocol = Com.Wui.Framework.Commons.Interfaces.IFileSystemItemProtocol;
    import FileSystemDownloadOptions = Com.Wui.Framework.Services.Connectors.FileSystemDownloadOptions;
    import ArchiveOptions = Com.Wui.Framework.Services.Connectors.ArchiveOptions;
    import ShortcutOptions = Com.Wui.Framework.Services.Connectors.ShortcutOptions;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
    import Extern = Com.Wui.Framework.Localhost.Primitives.Extern;

    export class FileSystemHandler extends Com.Wui.Framework.Localhost.Connectors.FileSystemHandler {

        @Extern()
        public Exists($path : string) : boolean {
            return super.Exists($path);
        }

        @Extern()
        public IsEmpty($path : string) : boolean {
            return super.IsEmpty($path);
        }

        @Extern()
        public IsFile($path : string) : boolean {
            return super.IsFile($path);
        }

        @Extern()
        public IsDirectory($path : string) : boolean {
            return super.IsDirectory($path);
        }

        @Extern()
        public IsSymbolicLink($path : string) : boolean {
            return super.IsSymbolicLink($path);
        }

        @Extern()
        public getTempPath() : string {
            return super.getTempPath();
        }

        @Extern()
        public Expand($pattern : string | string[]) : string[] {
            return super.Expand($pattern);
        }

        @Extern()
        public CreateDirectory($path : string) : boolean {
            return super.CreateDirectory($path);
        }

        @Extern()
        public Rename($oldPath : string, $newPath : string) : boolean {
            return super.Rename($oldPath, $newPath);
        }

        @Extern()
        public Read($path : string) : string | Buffer {
            return super.Read($path);
        }

        @Extern()
        public Write($path : string, $data : any, $append : boolean = false) : boolean {
            return super.Write($path, $data, $append);
        }

        @Extern()
        public Copy($sourcePath : string, $destinationPath : string, $callback? : (($status : boolean) => void) | IResponse) : void {
            super.Copy($sourcePath, $destinationPath, $callback);
        }

        @Extern()
        public Download($urlOrOptions : string | FileSystemDownloadOptions,
                        $callback? : (($headers : string, $bodyOrPath : string) => void) | IResponse) : void {
            super.Download($urlOrOptions, $callback);
        }

        @Extern()
        public AbortDownload($id : number) : boolean {
            return super.AbortDownload($id);
        }

        @Extern()
        public Delete($path : string, $callback? : (($status : boolean) => void) | IResponse) : boolean {
            return super.Delete($path, $callback);
        }

        @Extern()
        public Pack($path : string | string[], $options? : ArchiveOptions, $callback? : (($tmpPath : string) => void) | IResponse) : void {
            super.Pack($path, $options, $callback);
        }

        @Extern()
        public Unpack($path : string, $options? : ArchiveOptions, $callback? : (($tmpPath : string) => void) | IResponse) : void {
            super.Unpack($path, $options, $callback);
        }

        @Extern()
        public NormalizePath($source : string, $osSeparator : boolean = false) : string {
            return super.NormalizePath($source, $osSeparator);
        }

        @Extern()
        public getPathMap($path : string, $callback : (($map : IFileSystemItemProtocol[]) => void) | IResponse) : void {
            const driveList : any = require("drivelist");
            try {
                driveList.list(($error : Error, $drives : any[]) : void => {
                    if ($error) {
                        LogIt.Info($error.message);
                    }
                    let map : any[] = [];
                    try {
                        let pathParts : string[] = [];
                        $path = this.NormalizePath($path);
                        if (!ObjectValidator.IsEmptyOrNull($path)) {
                            pathParts = StringUtils.Split($path, "/");
                        }
                        $drives.forEach(($drive : any) : void => {
                            $drive.mountpoints.forEach(($disk : any) : void => {
                                const diskItem : IFileSystemItemProtocol = <any>{
                                    map  : [],
                                    name : $drive.description === "" ? "Local Disk" : $drive.description,
                                    type : "Drive",
                                    value: $disk.path
                                };
                                if (pathParts.length > 0 && pathParts[0] === $disk.path) {
                                    let lastMap : IFileSystemItemProtocol[] = null;
                                    let requiredPath : string = "";
                                    for (const part of pathParts) {
                                        if (part !== "") {
                                            if (requiredPath !== "" && !StringUtils.EndsWith(requiredPath, "/")) {
                                                requiredPath += "/";
                                            }
                                            requiredPath += part;
                                            const map : IFileSystemItemProtocol[] = this.getDirectoryContent(requiredPath);
                                            if (map !== []) {
                                                if (lastMap === null) {
                                                    diskItem.map = map;
                                                    lastMap = <IFileSystemItemProtocol[]>diskItem.map;
                                                } else {
                                                    lastMap.forEach(($item : IFileSystemItemProtocol) : void => {
                                                        if ($item.name === part) {
                                                            $item.map = map;
                                                            lastMap = <IFileSystemItemProtocol[]>$item.map;
                                                        }
                                                    });
                                                }
                                            } else {
                                                break;
                                            }
                                        }
                                    }
                                }
                                map.push(diskItem);
                            });
                        });
                    } catch (ex) {
                        LogIt.Warning(ex.message);
                    }

                    map = <any>[
                        {
                            map,
                            name: "Computer",
                            type: "MyComputer"
                        }
                    ];
                    ResponseFactory.getResponse($callback).Send(map);
                });
            } catch (ex) {
                LogIt.Warning("Unable to get drive info: " + ex.message);
                ResponseFactory.getResponse($callback).Send([]);
            }
        }

        @Extern()
        public getDirectoryContent($path : string) : IFileSystemItemProtocol[] {
            const fs : Types.NodeJS.fs = require("fs");
            const hidefile : any = require("hidefile");
            $path = this.NormalizePath($path);
            const map : IFileSystemItemProtocol[] = [];
            try {
                if (this.Exists($path)) {
                    try {
                        this.Expand($path + "/*").forEach(($file : string) : void => {
                            try {
                                const item : IFileSystemItemProtocol = <IFileSystemItemProtocol>{
                                    name: $file,
                                    type: "File"
                                };

                                const attributes : string[] = [];
                                const stats : any = fs.statSync($path + "/" + $file);
                                try {
                                    /* tslint:disable: no-bitwise */
                                    if ((stats.mode & 146) === 0) {
                                        attributes.push("Readonly");
                                    }
                                    if ((stats.mode & 4000) === 1 || hidefile.isHiddenSync($path + "/" + $file)) {
                                        attributes.push("Hidden");
                                    }
                                    /* tslint:enable */

                                    if (stats.isFile()) {
                                        item.type = "File";
                                    } else if (stats.isDirectory()) {
                                        item.type = "Directory";
                                    } else if (stats.isSymbolicLink() || StringUtils.Contains(item.name, ".lnk")) {
                                        item.type = "Link";
                                    }
                                } catch (ex) {
                                    LogIt.Warning(ex.message);

                                    attributes.push("PermissionDenied");
                                    if (stats.isDirectory()) {
                                        item.type = "Directory";
                                    } else if (stats.isSymbolicLink() || StringUtils.Contains(item.name, ".lnk")) {
                                        item.type = "Link";
                                    }
                                }

                                if (attributes.length > 0) {
                                    item.attributes = attributes;
                                }
                                map.push(item);
                            } catch (ex) {
                                LogIt.Warning(ex.message);
                            }
                        });
                    } catch (ex) {
                        LogIt.Warning(ex.message);
                    }
                    map.forEach(($directory : IFileSystemItemProtocol) : void => {
                        if ($directory.type === "Directory") {
                            $directory.map = this.IsEmpty($path + "/" + $directory.name) ? [] : "***";
                        }
                    });
                }
            } catch (ex) {
                LogIt.Warning(ex.message);
            }
            return map;
        }

        @Extern()
        public CreateShortcut($source : string, $destination : string, $options : ShortcutOptions,
                              $callback : (($status : boolean) => void) | IResponse) : void {
            const path : Types.NodeJS.path = require("path");
            LogIt.Info("> create shortcut for \"" + $source + "\" at \"" + $destination + "\"");
            if (ObjectValidator.IsEmptyOrNull($callback)) {
                $callback = <any>$options;
                $options = {};
            }
            const response : IResponse = ResponseFactory.getResponse($callback);
            if (EnvironmentHelper.IsWindows()) {
                try {
                    const options : any = !ObjectValidator.IsEmptyOrNull($options) ? $options : {};
                    options.target = this.NormalizePath($source);
                    if (options.hasOwnProperty("args") && !ObjectValidator.IsString(options.args)) {
                        options.args = options.args.join(" ");
                    }
                    if (!StringUtils.EndsWith($destination, ".lnk")) {
                        $destination += ".lnk";
                    }
                    $destination = this.NormalizePath($destination);
                    const folder : string = path.dirname($destination);
                    this.CreateDirectory(folder);

                    const method : string = this.Exists($destination) ? "edit" : "create";
                    require("windows-shortcuts")[method]($destination, $options, ($error : string) : void => {
                        if ($error) {
                            response.OnError($error);
                        } else {
                            response.Send(true);
                        }
                    });
                } catch (ex) {
                    response.OnError(ex);
                }
            } else {
                response.OnError("CreateShortcut method is supported only on Windows environment.");
            }
        }

        @Extern()
        public ReadStream($path : string) : string {
            return (<any>this.Read($path)).toString("base64");
        }

        @Extern()
        public WriteStream($path : string, $data : string, $append : boolean = false) : boolean {
            return this.Write($path, Buffer.from($data, "base64"), $append);
        }

        public Resolve($args : any, $response : IResponse) : void {
            switch ($args.type) {
            case "Exists":
                $response.Send(this.Exists($args.value));
                break;
            case "getTempPath":
                $response.Send(this.getTempPath());
                break;
            case "CreateDirectory":
                $response.Send(this.CreateDirectory($args.value));
                break;
            case "Rename":
                $response.Send(this.Rename($args.oldValue, $args.value));
                break;
            case "Write":
                $response.Send(this.Write($args.path, $args.value, $args.append));
                break;
            case "Read":
                $response.Send(this.Read($args.value).toString());
                break;
            case "Delete":
                $response.Send(this.Delete($args.value));
                break;
            case "Copy":
                this.Copy($args.sourceValue, $args.value, $response);
                break;
            default:
                $response.OnError("Unrecognized FileSystem request type \"" + $args.type + "\"");
                break;
            }
        }
    }
}
