/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Connector.Interfaces {
    "use strict";

    export abstract class IProject extends Com.Wui.Framework.Localhost.Interfaces.IProject {
        public agent : IAgentConfiguration;
        public hubLocations : IHubLocations;
    }

    export abstract class IAgentConfiguration {
        public name : string;
        public hub : string;
    }

    export abstract class IHubLocations {
        public prod : string;
        public eap : string;
        public dev : string;
    }
}
