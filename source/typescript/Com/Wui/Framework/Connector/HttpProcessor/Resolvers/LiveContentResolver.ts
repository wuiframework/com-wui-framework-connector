/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Connector.HttpProcessor.Resolvers {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;

    export class LiveContentResolver extends Com.Wui.Framework.Localhost.HttpProcessor.Resolvers.LiveContentResolver {

        protected resolveCustomProtocol($type : string, $args : any, $response : IResponse) : void {
            switch ($type) {
            case "Cmd":
            case "Spawn":
                Loader.getInstance().getTerminal().Spawn($args.cmd, $args.args, $args.cwd, $response);
                break;

            case "FileSystem":
            case "FileSystemHandler":
                Loader.getInstance().getFileSystemHandler().Resolve($args, $response);
                break;

            default:
                super.resolveCustomProtocol($type, $args, $response);
                break;
            }
        }
    }
}
