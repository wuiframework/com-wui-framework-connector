/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Connector.HttpProcessor.Resolvers {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import HttpStatusType = Com.Wui.Framework.Commons.Enums.HttpStatusType;
    import IResponseProtocol = Com.Wui.Framework.Localhost.Interfaces.IResponseProtocol;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class ConnectorResponse extends Com.Wui.Framework.Localhost.HttpProcessor.Resolvers.ConnectorResponse {

        protected resolver() : void {
            if (StringUtils.Contains(this.RequestArgs().Url(), "/connector.config.js")) {
                const extension : string = Loader.getInstance().getProgramArgs().IsDebug() ? ".js" : ".jsonp";
                this.getConnector().Send(<IResponseProtocol>{
                    body   : require("fs").createReadStream(
                        Loader.getInstance().getProgramArgs().ProjectBase() + "/connector.config" + extension),
                    headers: {"content-type": "application/javascript"},
                    status : HttpStatusType.SUCCESS
                });
            } else {
                super.resolver();
            }
        }
    }
}
