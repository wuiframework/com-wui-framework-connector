/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Connector.HttpProcessor {
    "use strict";
    import IProject = Com.Wui.Framework.Connector.Interfaces.IProject;
    import ProgramArgs = Com.Wui.Framework.Connector.Structures.ProgramArgs;
    import Network = Com.Wui.Framework.Connector.Connectors.Network;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import HttpServerEventArgs = Com.Wui.Framework.Localhost.Events.Args.HttpServerEventArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class HttpServer extends Com.Wui.Framework.Localhost.HttpProcessor.HttpServer {

        public Start() : void {
            const args : ProgramArgs = Loader.getInstance().getProgramArgs();
            const networking : Network = new Network();
            const clients : number[] = [];

            const startService : any = () : void => {
                const createServer : any = ($port : number) : void => {
                    const patterns : string[] = [];
                    args.ConfigLocations().forEach(($path : string) : void => {
                        $path = StringUtils.Replace($path, "\\", "/");
                        if (StringUtils.Contains($path, "/resource/")) {
                            $path = StringUtils.Substring($path, 0, StringUtils.IndexOf($path, "/resource/"));
                        }
                        if (StringUtils.Contains($path, "/build/target")) {
                            $path = StringUtils.Substring($path, 0, StringUtils.IndexOf($path, "/build/target"));
                        }
                        if (patterns.indexOf($path) === -1) {
                            patterns.push($path);
                        }
                    });
                    if (ObjectValidator.IsEmptyOrNull(patterns)) {
                        this.getEvents().OnConnect(($args : HttpServerEventArgs) : void => {
                            if (clients.indexOf($args.ClientId()) === -1) {
                                clients.push($args.ClientId());
                            }
                        });
                        this.getEvents().OnDisconnect(($args : HttpServerEventArgs) : void => {
                            if (clients.indexOf($args.ClientId()) !== -1) {
                                clients.splice(clients.indexOf($args.ClientId()), 1);
                            }
                            if (clients.length === 0 && !args.IsDebug()) {
                                Loader.getInstance().Exit();
                            }
                        });
                    } else {
                        const anonymize : any = {};
                        for (let index : number = 0; index < patterns.length; index++) {
                            anonymize["target" + index] = patterns[index];
                        }
                        Loader.getInstance().setAnonymizer(anonymize);
                    }
                    this.createServer($port, false, true, [args.ProjectBase()].concat(args.ConfigLocations()));
                };
                networking.IsPortFree(args.ServicesPort(), ($status : boolean) : void => {
                    if (!$status) {
                        networking.getFreePort(($port : number) : void => {
                            createServer($port);
                        });
                    } else {
                        createServer(args.ServicesPort());
                    }
                });
            };
            if (args.OpenHost()) {
                const hostPorts : number[] = [80, 8080, 8880, 8088, 8008];
                if (args.HostPort() <= 0) {
                    LogIt.Debug("Specified port is null or negative, switched to automatic selection from default ports.");
                } else {
                    hostPorts.unshift(args.HostPort());
                }
                const choosePort : any = ($hostPortIndex : number) : void => {
                    if ($hostPortIndex < hostPorts.length) {
                        networking.IsPortFree(hostPorts[$hostPortIndex], ($status : boolean) : void => {
                            if (!$status) {
                                LogIt.Debug("Host port " + hostPorts[$hostPortIndex] + " is locked, trying next...");
                                choosePort($hostPortIndex + 1);
                            } else {
                                this.createServer(hostPorts[$hostPortIndex], false, false);
                                startService();
                            }
                        });
                    } else {
                        LogIt.Error("Host can not be started. No available port for host server.");
                        startService();
                    }
                };
                choosePort(0);
            } else {
                startService();
            }
        }

        protected getAppConfiguration() : IProject {
            return <IProject>super.getAppConfiguration();
        }
    }
}
