/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Connector {
    "use strict";
    import HttpResolver = Com.Wui.Framework.Connector.HttpProcessor.HttpResolver;
    import IProject = Com.Wui.Framework.Connector.Interfaces.IProject;
    import HttpServer = Com.Wui.Framework.Connector.HttpProcessor.HttpServer;
    import ProgramArgs = Com.Wui.Framework.Connector.Structures.ProgramArgs;
    import FileSystemHandler = Com.Wui.Framework.Connector.Connectors.FileSystemHandler;
    import Terminal = Com.Wui.Framework.Connector.Connectors.Terminal;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import WuiHubConnector = Com.Wui.Framework.Connector.Connectors.WuiHubConnector;
    import EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
    import AsyncRequestEventArgs = Com.Wui.Framework.Commons.Events.Args.AsyncRequestEventArgs;
    import IResponse = Com.Wui.Framework.Localhost.Interfaces.IResponse;
    import WebResponse = Com.Wui.Framework.Localhost.HttpProcessor.ResponseApi.Handlers.WebResponse;
    import IRequestConnector = Com.Wui.Framework.Localhost.Interfaces.IRequestConnector;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Metrics = Com.Wui.Framework.Localhost.Utils.Metrics;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class Loader extends Com.Wui.Framework.Localhost.Loader {

        public static getInstance() : Loader {
            return <Loader>super.getInstance();
        }

        public getAppConfiguration() : IProject {
            return <IProject>super.getAppConfiguration();
        }

        public getHttpResolver() : HttpResolver {
            return <HttpResolver>super.getHttpResolver();
        }

        public getProgramArgs() : ProgramArgs {
            return <ProgramArgs>super.getProgramArgs();
        }

        public getFileSystemHandler() : FileSystemHandler {
            return <FileSystemHandler>super.getFileSystemHandler();
        }

        public getTerminal() : Terminal {
            return <Terminal>super.getTerminal();
        }

        protected initProgramArgs() : ProgramArgs {
            return new ProgramArgs();
        }

        protected initHttpServer() : HttpServer {
            return new HttpServer();
        }

        protected initResolver() : HttpResolver {
            return new HttpResolver("/");
        }

        protected initFileSystem() : FileSystemHandler {
            return new FileSystemHandler();
        }

        protected initTerminal() : Terminal {
            return new Terminal();
        }

        protected processProgramArgs() : boolean {
            const args : ProgramArgs = this.getProgramArgs();
            if (args.IsAgent()) {
                this.getAppConfiguration().target.metrics.enabled = true;
                Metrics.getInstance().Instrument([
                    Com.Wui.Framework.Connector.Connectors
                ]);
                args.StartServer(false);
                super.processProgramArgs();
                args.TargetBase(args.ProjectBase());

                if (!ObjectValidator.IsEmptyOrNull(args.HubLocation())) {
                    this.getAppConfiguration().agent.hub = args.HubLocation();
                }
                const hubLocation : string = StringUtils.ToLowerCase(this.getAppConfiguration().agent.hub);
                if (this.getAppConfiguration().hubLocations.hasOwnProperty(hubLocation)) {
                    this.getAppConfiguration().agent.hub = this.getAppConfiguration().hubLocations[hubLocation];
                }
                const connector : WuiHubConnector = new WuiHubConnector();
                let platform : string = "unrecognized";
                if (EnvironmentHelper.IsWindows()) {
                    platform = "win";
                } else if (EnvironmentHelper.IsLinux()) {
                    if (EnvironmentHelper.IsArm()) {
                        platform = "arm";
                    } else {
                        platform = "linux";
                    }
                } else if (EnvironmentHelper.IsMac()) {
                    platform = "mac";
                }
                platform += EnvironmentHelper.Is64bit() ? "64" : "32";
                LogIt.Debug("Sending registration request for WUI Connector agent to WUI Hub [{0}] ...",
                    this.getAppConfiguration().agent.hub);
                const domain : string = this.getHttpManager().getRequest().getServerIP();
                let name : string = this.getEnvironmentArgs().getAppName();
                if (!ObjectValidator.IsEmptyOrNull(this.getAppConfiguration().agent.name)) {
                    name = this.getAppConfiguration().agent.name;
                }
                if (!ObjectValidator.IsEmptyOrNull(args.AgentName())) {
                    name = args.AgentName();
                }
                connector.getEvents().OnStart(() : void => {
                    connector
                        .RegisterAgent({
                            domain,
                            name,
                            platform,
                            version: this.getEnvironmentArgs().getProjectVersion()
                        })
                        .OnMessage(($data : any, $taskId : string) : void => {
                            const proxyConnector : IRequestConnector = {
                                Send      : ($data : any) : void => {
                                    connector.ForwardMessage($taskId, $data);
                                },
                                getOwnerId: () : string => {
                                    return connector.getId() + "";
                                }
                            };
                            const response : IResponse = new WebResponse(proxyConnector, $data);
                            const args : AsyncRequestEventArgs = new AsyncRequestEventArgs("/liveContent/");
                            args.POST().Add($data, "LiveProtocol");
                            args.POST().Add(response, "LiveResponse");
                            args.POST().Add(JSON.stringify($data), "Protocol");
                            args.POST().Add(proxyConnector, "Connector");
                            this.getHttpResolver().ResolveRequest(args);
                        })
                        .Then(($status : boolean) : void => {
                            if (!$status) {
                                LogIt.Warning("Unable to register agent!");
                                this.Exit(-1);
                            } else {
                                LogIt.Info("Agent registered.");
                            }
                        });
                });
                connector.StartCommunication();
                return true;
            } else {
                if (args.HostPort() === args.ServicesPort()) {
                    LogIt.Info("Host and services ports are identical, using default values");
                    args.ServicesPort(8888);
                    args.HostPort(88);
                }
                return super.processProgramArgs();
            }
        }
    }
}
