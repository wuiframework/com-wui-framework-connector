/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Connector.Structures {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class ProgramArgs extends Com.Wui.Framework.Localhost.Structures.ProgramArgs {
        private hostPort : number;
        private servicesPort : number;
        private openHost : boolean;
        private configLocations : string[];
        private isAgent : boolean;
        private agentName : string;
        private hubLocation : string;

        constructor() {
            super();
            this.hostPort = 0;
            this.servicesPort = 8888;
            this.openHost = false;
            this.configLocations = [];
            this.isAgent = false;
            this.agentName = "";
            this.hubLocation = "";
            this.StartServer(true);
        }

        public HostPort($value? : number) : number {
            return this.hostPort = Property.PositiveInteger(this.hostPort, $value, 1);
        }

        public ServicesPort($value? : number) : number {
            return this.servicesPort = Property.PositiveInteger(this.servicesPort, $value, 1);
        }

        public OpenHost($value? : boolean) : boolean {
            return this.openHost = Property.Boolean(this.openHost, $value);
        }

        public ConfigLocations($value? : string[]) : string[] {
            if (ObjectValidator.IsSet($value)) {
                this.configLocations = $value;
            }
            return this.configLocations;
        }

        public IsAgent($value? : boolean) : boolean {
            return this.isAgent = Property.Boolean(this.isAgent, $value);
        }

        public AgentName($value? : string) : string {
            return this.agentName = Property.String(this.agentName, $value);
        }

        public HubLocation($value? : string) : string {
            return this.hubLocation = Property.String(this.hubLocation, $value);
        }

        public PrintHelp() : void {
            this.getHelp(
                "" +
                "WUI Framework Connector                                                      \n" +
                "   - Standalone HTTP server for with built-in web host and WebSockets support\n" +
                "                                                                             \n" +
                "   copyright:         Copyright (c) 2014-2016 Freescale Semiconductor, Inc., \n" +
                "                      Copyright (c) 2017-2019 NXP                            \n" +
                "   author:            Jakub Cieslar, jakub.cieslar@nxp.com                   \n" +
                "   license:           BSD-3-Clause License distributed with this material    \n",

                "" +
                "Basic options:                                                               \n" +
                "   -h [ --help ]               Prints application help description.          \n" +
                "   -v [ --version ]            Prints application version message.           \n" +
                "   -p [ --path ]               Print current WUI Connector location.         \n" +
                "                                                                             \n" +
                "Server options:                                                              \n" +
                "   --stop [ -s ]               Stop localhost service.                       \n" +
                "   --open-host [ -o ]          Use this to open host server.                 \n" +
                "   agent                       Connect WUI Connector to WUI Hub.             \n" +
                "                                                                             \n" +
                "Other options:                                                               \n" +
                "   --services-port=<value>     Specify server services port.                 \n" +
                "   --host-port=<value>         Specify host port.                            \n" +
                "   --name=<value>              Specify name used by WUI Hub registration.    \n" +
                "   --hub=<value>               Specify type or url for WUI Hub.              \n" +
                "   --target=<path>             Specify target *.html file path.              \n" +
                "   --config-locations=<paths>  Specify list of connector.config.jsonp        \n" +
                "                                 locations separated by ';'.                 \n"
            );
        }

        protected processDefaultArgs() : void {
            this.StartServer(true);
        }

        protected processArg($key : string, $value : string) : boolean {
            switch ($key) {
            case "--stop":
            case "-s":
                this.StopServer(true);
                break;
            case "--open-host":
            case "-o":
                this.OpenHost(true);
                break;
            case "--services-port":
                this.ServicesPort(StringUtils.ToInteger($value));
                break;
            case "--host-port":
                this.HostPort(StringUtils.ToInteger($value));
                break;
            case "--config-locations":
                if (StringUtils.Contains($value, ";")) {
                    this.ConfigLocations(StringUtils.Split($value, ";"));
                } else {
                    this.ConfigLocations([$value]);
                }
                break;
            case "agent":
                this.IsAgent(true);
                break;
            case "--name":
                this.AgentName($value);
                break;
            case "--hub":
                this.HubLocation($value);
                break;
            default:
                return super.processArg($key, $value);
            }
            return true;
        }
    }
}
