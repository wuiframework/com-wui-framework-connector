/* tslint:disable:no-reference */
/// <reference path="../../dependencies/com-wui-framework-commons/source/typescript/reference.d.ts" />
/// <reference path="../../dependencies/com-wui-framework-gui/source/typescript/reference.d.ts" />
/// <reference path="../../dependencies/com-wui-framework-services/source/typescript/reference.d.ts" />
/// <reference path="../../dependencies/com-wui-framework-localhost/source/typescript/reference.d.ts" />

// generated-code-start
/// <reference path="Com/Wui/Framework/Connector/Interfaces/IProject.ts" />
/// <reference path="interfacesMap.ts" />
/// <reference path="Com/Wui/Framework/Connector/DAO/Resources.ts" />
/// <reference path="Com/Wui/Framework/Connector/HttpProcessor/HttpResolver.ts" />
/// <reference path="Com/Wui/Framework/Connector/HttpProcessor/Resolvers/LiveContentResolver.ts" />
/// <reference path="Com/Wui/Framework/Connector/Connectors/Terminal.ts" />
/// <reference path="Com/Wui/Framework/Connector/RuntimeTests/Snippets.ts" />
/// <reference path="Com/Wui/Framework/Connector/Index.ts" />
/// <reference path="Com/Wui/Framework/Connector/HttpProcessor/Resolvers/ConnectorResponse.ts" />
/// <reference path="Com/Wui/Framework/Connector/Structures/ProgramArgs.ts" />
/// <reference path="Com/Wui/Framework/Connector/Connectors/Network.ts" />
/// <reference path="Com/Wui/Framework/Connector/Connectors/FileSystemHandler.ts" />
/// <reference path="Com/Wui/Framework/Connector/Connectors/WuiHubConnector.ts" />
/// <reference path="Com/Wui/Framework/Connector/Connectors/FFIProxy.ts" />
/// <reference path="Com/Wui/Framework/Connector/HttpProcessor/HttpServer.ts" />
/// <reference path="Com/Wui/Framework/Connector/Loader.ts" />
// generated-code-end
