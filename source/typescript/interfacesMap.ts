/** WARNING: this file has been automatically generated from typescript interfaces, which exist in this package. */

/* tslint:disable: variable-name no-use-before-declare only-arrow-functions */
namespace Com.Wui.Framework.Connector.Connectors {
    "use strict";
    export let IConnectorHubRegisterPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "OnMessage"
            ], Com.Wui.Framework.Services.Connectors.IAcknowledgePromise);
        }();
}

/* tslint:enable */
