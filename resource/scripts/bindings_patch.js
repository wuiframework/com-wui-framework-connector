/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

module.exports = function ($opts) {
    switch ($opts) {
        case "VersionInfo":
            return require(process.nodejsRoot + "/resource/libs/win-version-info/VersionInfo.node");
        case "ref":
        case "fastcall":
            return require(process.nodejsRoot + "/resource/libs/fastcall/" + $opts + ".node");
        case "drivelist":
            return require(process.nodejsRoot + "/resource/libs/drivelist/drivelist.node");
        default:
            throw new Error("Unsupported binding patch for: " + JSON.stringify($opts));
    }
};
