/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
const Loader = Com.Wui.Framework.Builder.Loader;
const LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
const ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
const EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
const DependenciesInstall = Com.Wui.Framework.Builder.Tasks.Composition.DependenciesInstall;
const OSType = Com.Wui.Framework.Builder.Enums.OSType;
const Patcher = Com.Wui.Framework.Builder.Utils.Patcher;
const terminal = Loader.getInstance().getTerminal();
const filesystem = Loader.getInstance().getFileSystemHandler();
let cwd;

function finalize($done) {
    Patcher.getInstance().Rollback($done);
}

function fail($info) {
    finalize(() => {
        LogIt.Error($info);
    });
}

function validate($done) {
    if (filesystem.Exists(cwd + "/build/Release/ref.node") && filesystem.Exists(cwd + "/build/Release/fastcall.node")) {
        CheckModule([cwd + "/build/Release/ref.node", cwd + "/build/Release/fastcall.node"], $done);
    } else {
        $done(false);
    }
}

function build($done) {
    Patcher.getInstance().PatchMultiKeys(cwd + "/CMakeLists.txt",
        {
            "-march=native": ""
        },
        () => {
            const env = process.env;
            env.NODEJSRE_SKIP_LOADER = 1;
            env.NPM_CONFIG_PREFIX = "/tmp/nodejs";
            env.CC = "/usr/bin/aarch64-linux-gnu-gcc-7";
            env.CXX = "/usr/bin/aarch64-linux-gnu-g++-7";
            terminal.Spawn("wui",
                [cwd + "/../cmake-js/bin/cmake-js", "--arch=aarch64", "rebuild"],
                {cwd, env},
                ($exitCode) => {
                    if ($exitCode === 0) {
                        validate(($status) => {
                            if ($status) {
                                finalize($done);
                            } else {
                                fail("Fastcall validation failed.");
                            }
                        });
                    } else {
                        fail("Can not rebuild fastcall");
                    }
                });
        });
}

Process = function ($cwd, $args, $done) {
    cwd = $cwd;
    if (DependenciesInstall.getCurrentInstallOsType() === OSType.IMX) {
        validate(($status) => {
            if ($status !== true) {
                LogIt.Info("Starting fastcall build...");
                build($done);
            } else {
                LogIt.Info("Fastcall is ready.");
                finalize($done);
            }
        })
    } else {
        LogIt.Info("Fastcall build script nothing to build on " +
            DependenciesInstall.getCurrentInstallOsType() + " platform.");
        finalize($done);
    }
};
